﻿using Gigaming.IGAMING.Operator.API.Feature.GameLaunch.GameLaunchParameters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GameLaunchAPI.Helper
{
    public enum CasinoGame
    {
        Lobby = 0,
        Baccarat = 1,
        Roulette = 3,
        SicBo = 5,
        DragonTiger = 9,
        MultipleTableBaccarat = 10,
        BeautyBaccarat = 11,
        SpeedBaccarat = 12
    }

    public class SboGameHelper
    {
        public static Game CreateVirtualSportsGameParameters()
        {
            var sboVirtualSportGameLaunchParameter = new List<IDictionary<string, string>>
            {
                new Dictionary<string, string> { { "token", "token" } },
                new Dictionary<string, string> { { "lang", "en" } },
            };

            return new Game { Name = "VirtualSports", Parameters = sboVirtualSportGameLaunchParameter };
        }

        public static Game CreateCasinoGameParameters(CasinoGame game)
        {
            var sboCasinoGameLaunchParameter = new List<IDictionary<string, string>>
            {
                new Dictionary<string, string> { { "locale", "en" } },
                new Dictionary<string, string> { { "sb", "sports-winfast555.com" } },
                new Dictionary<string, string> { { "device", "d" } },
                new Dictionary<string, string> { { "loginMode", "2" } },
                new Dictionary<string, string> { { "productId", ((int)game).ToString() } }
            };

            return new Game { Name = Enum.GetName(typeof(CasinoGame), game), Parameters = sboCasinoGameLaunchParameter };
        }

        public static Game CreateSportsGameParameters()
        {
            var sboSportGameLaunchParameter = new List<IDictionary<string, string>>
            {
                new Dictionary<string, string> { { "lang", "en" } },
                new Dictionary<string, string> { { "oddstyle", "ID" } },
                new Dictionary<string, string> { { "theme", "black" } },
                new Dictionary<string, string> { { "oddsmode", "double" } }
            };

            return new Game { Name = "SportsBook", Parameters = sboSportGameLaunchParameter };
        }

        public static List<Game> CreateGameParameters()
        {
            var games = System.IO.File.ReadAllLines(@"Data/SboGames.txt");
            var gameList = new List<Dictionary<string, string>>();
            var result = new List<Game>();

            var sboSportGameLaunchParameter = new List<IDictionary<string, string>>
            {
                new Dictionary<string, string> { { "gameId", "en" } },
                new Dictionary<string, string> { { "token", "token" } },
            };

            foreach (var game in games)
            {
                var fields = game.Split("\t", 2);
                gameList.Add(new Dictionary<string, string> { { fields[0], fields[1] } });
            }

            foreach (var game in gameList)
            {
                result.Add(new Game
                {
                    Name = game.Values.First(),
                    Parameters = new List<IDictionary<string, string>>
                    {
                        new Dictionary<string, string> { { "gameId", game.Keys.First() } },
                    }
                });
            }

            return result;
        }

        public static List<Game> CreateSlotGameParameters()
        {
            var games = System.IO.File.ReadAllLines(@"Data/SboSlotGames.txt");
            var gameList = new List<Dictionary<string, string>>();
            var result = new List<Game>();

            var sboSportGameLaunchParameter = new List<IDictionary<string, string>>
            {
                new Dictionary<string, string> { { "gameId", "en" } },
                new Dictionary<string, string> { { "token", "token" } },
            };

            foreach (var game in games)
            {
                var fields = game.Split("\t", 2);
                gameList.Add(new Dictionary<string, string> { { fields[0], fields[1] } });
            }

            foreach (var game in gameList)
            {
                result.Add(new Game
                {
                    Name = game.Values.First(),
                    Parameters = new List<IDictionary<string, string>>
                    {
                        new Dictionary<string, string> { { "gameId", game.Keys.First() } },
                    }
                });
            }

            return result;
        }
    }
}
