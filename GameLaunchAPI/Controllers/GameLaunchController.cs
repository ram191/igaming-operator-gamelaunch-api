﻿using System.Threading.Tasks;
using Gigaming.IGAMING.Operator.API.Feature.GameLaunch.GameLaunchParameters;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GameLaunchAPI.Controllers
{
    [ApiController]
    [Route("/")]
    public class GameLaunchController : ControllerBase
    {
        private readonly IMediator _mediator;

        public GameLaunchController(ILogger<GameLaunchController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet("gameParameters")]
        public async Task<IActionResult> GetGameParameters()
        {
            var result = await _mediator.Send(new Request());
            return Ok(result);
        }
    }
}
