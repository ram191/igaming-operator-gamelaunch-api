﻿using GameLaunchAPI.Helper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Gigaming.IGAMING.Operator.API.Feature.GameLaunch.GameLaunchParameters
{
    public class Handler : IRequestHandler<Request, Response>
    {
        public async Task<Response> Handle(Request request, CancellationToken cancellationToken)
        {
            await Task.Delay(10);

            //List of Games
            var sboCasinoGames = new List<Game>
            {
                SboGameHelper.CreateCasinoGameParameters(CasinoGame.Baccarat),
                SboGameHelper.CreateCasinoGameParameters(CasinoGame.BeautyBaccarat),
                SboGameHelper.CreateCasinoGameParameters(CasinoGame.DragonTiger),
                SboGameHelper.CreateCasinoGameParameters(CasinoGame.Lobby),
                SboGameHelper.CreateCasinoGameParameters(CasinoGame.MultipleTableBaccarat),
                SboGameHelper.CreateCasinoGameParameters(CasinoGame.Roulette),
                SboGameHelper.CreateCasinoGameParameters(CasinoGame.SicBo),
                SboGameHelper.CreateCasinoGameParameters(CasinoGame.SpeedBaccarat),
            };

            var sportsGame = new List<Game>
            {
                SboGameHelper.CreateSportsGameParameters()
            };

            var virtualSportsGame = new List<Game>
            {
                SboGameHelper.CreateVirtualSportsGameParameters()
            };

            //Objects
            var liveCasino = new List<GameProvider>
            {
                new GameProvider
                {
                    Domain = "casino-winfast555.com",
                    ProviderName = "SBO",
                    Games = sboCasinoGames
                }
            };

            var sports = new List<GameProvider>
            {
                new GameProvider
                {
                    Domain = "sports-winfast555.com",
                    ProviderName = "SBO",
                    Games = sportsGame
                }
            };

            var games = new List<GameProvider>
            {
                new GameProvider
                {
                    Domain = "game-winfast555.com",
                    ProviderName = "SBO",
                    Games = SboGameHelper.CreateGameParameters()
                }
            };

            var slots = new List<GameProvider>
            {
                new GameProvider
                {
                    Domain = "game-winfast555.com",
                    ProviderName = "SBO",
                    Games = SboGameHelper.CreateSlotGameParameters()
                }
            };

            var virtualSport = new List<GameProvider>
            {
                new GameProvider
                {
                    Domain = "virtualsports-winfast555.com",
                    ProviderName = "SBO",
                    Games = virtualSportsGame
                }
            };

            return new Response
            {
                LiveCasino = liveCasino,
                SportsBook = sports,
                Games = games,
                Slots = slots,
                VirtualSports = virtualSport
            };
        }
    }
}
