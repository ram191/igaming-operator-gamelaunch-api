﻿using System.Collections.Generic;


namespace Gigaming.IGAMING.Operator.API.Feature.GameLaunch.GameLaunchParameters
{
    public class Response
    {
        public List<GameProvider> SportsBook { get; set; }
        public List<GameProvider> VirtualSports { get; set; }
        public List<GameProvider> LiveCasino { get; set; }
        public List<GameProvider> Games { get; set; }
        public List<GameProvider> Slots { get; set; }
    }

    public class GameProvider
    {
        public string ProviderName { get; set; }
        public string Domain { get; set; }
        public List<Game> Games { get; set; }
    }

    public class Game
    {
        public string Name { get; set; }
        public List<IDictionary<string, string>> Parameters { get; set; }
    }
}